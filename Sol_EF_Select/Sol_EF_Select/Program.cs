﻿using Sol_EF_Select.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Select
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async() => {

                IEnumerable<UserEntity> listUserEntity =
                    await new UserDal().GetUserData();

            }).Wait();
        }
    }
}
