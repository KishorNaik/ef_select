﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Select.Entities
{
    public class UserEntity
    {
        public int UserId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }
    }
}
