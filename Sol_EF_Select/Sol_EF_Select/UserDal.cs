﻿using Sol_EF_Select.EF;
using Sol_EF_Select.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity; // for Async Linq Extension Method
using System.Linq.Expressions;

namespace Sol_EF_Select
{
    public class UserDal
    {
        #region Declaration
        private UserDBEntities db = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            db = new UserDBEntities();
        }
        #endregion

        #region Public Method
        public async Task<IEnumerable<UserEntity>> GetUserData()
        {
            try
            {




                //// using Linq Query

                //var getQuery1 =
                //        from Q in db.tblUsers
                //        select new UserEntity()
                //        {
                //            UserId = (int)Q.UserId,
                //            FirstName = Q.FirstName,
                //            LastName = Q.LastName
                //        };

                //return await getQuery1.ToListAsync();

                // using Method Base

                //var getQuery = (await
                //        db
                //        ?.tblUsers
                //        ?.ToListAsync()
                //        )
                //        ?.AsEnumerable()
                //        ?.Select((letblUser)=> new UserEntity() {
                //            UserId=(int)letblUser.UserId,
                //            FirstName=letblUser.FirstName,
                //            LastName=letblUser.LastName
                //        });

                //var getQuery =
                //    await
                //        db
                //        ?.tblUsers
                //        ?.AsQueryable()
                //        ?.Select((letblUserObj) => new UserEntity()
                //        {
                //            UserId = (int)letblUserObj.UserId,
                //            FirstName = letblUserObj.FirstName,
                //            LastName = letblUserObj.LastName
                //        })
                //        ?.ToListAsync();

                var getQuery =

                        db
                        ?.tblUsers
                        ?.AsQueryable()
                        ?.Select(this.SelectUserData)
                        ?.ToList();

                return getQuery;

            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        #region Property
        private Func<tblUser,UserEntity> SelectUserData
        {
            get
            {
                return
                       (letblUserObj) => new UserEntity()
                       {
                           UserId=(int)letblUserObj.UserId,
                           FirstName=letblUserObj.FirstName,
                           LastName=letblUserObj.LastName
                       };
            }
        }
        #endregion 
    }
}
